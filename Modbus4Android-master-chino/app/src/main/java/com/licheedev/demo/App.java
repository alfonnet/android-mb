package com.licheedev.demo;

import android.app.Application;
import com.licheedev.adaptscreen.AdaptScreenEx;
import com.licheedev.demo.base.PrefUtil;
import com.serotonin.modbus4j.ModbusConfig;

public class App extends Application {

    static App sInstance;

    @Override
    public void onCreate() {
        super.onCreate();
        sInstance = this;
        AdaptScreenEx.init(this);
        PrefUtil.init(this);
        configModbus();
    }

    public static App getInstance() {
        return sInstance;
    }

    /**
     * 配置Modbus,可选
     */
    private void configModbus() {
        ModbusConfig.setEnableRtuCrc(true);
        ModbusConfig.setEnableDataLog(true, true);
    }
}
