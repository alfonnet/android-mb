package com.licheedev.demo.modbus;

import android.content.BroadcastReceiver;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.hardware.usb.UsbManager;
import android.os.AsyncTask;
import android.os.SystemClock;
import android.serialport.SerialPortFinder;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;
import com.hoho.android.usbserial.BuildConfig;
import com.hoho.android.usbserial.driver.UsbSerialDriver;
import com.hoho.android.usbserial.driver.UsbSerialPort;
import com.hoho.android.usbserial.driver.UsbSerialProber;
import com.licheedev.demo.App;
import com.licheedev.demo.R;
import com.licheedev.demo.base.PrefUtil;
import com.serotonin.modbus4j.ModbusConfig;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.regex.Pattern;
import org.apache.commons.lang3.StringUtils;

/**
 * @ Created by John on 2018/3/29.
 */

public class DeviceConfig {


    private final String TAG = DeviceConfig.class.getSimpleName();

    public static class Config {

        String devicePath;
        int baudrate = 0;
        boolean enableCrc = true;
    }

    private static final String CONFIG = "serial_config";

    private static DeviceConfig sInstance = new DeviceConfig();

    private Gson mGson;
    private Config mConfig;
    // Lista de ruta del dispositivo
    private String[] mDevicePaths;
    // Lista de baudios
    private int[] mBaudrates;
    // Lista de cadenas de velocidad de transmisión
    private String[] mBaudrateStrs;


    private BroadcastReceiver mUsbReceiver;

    private List<UsbSerialPort> mEntries = new ArrayList<UsbSerialPort>();
    private ArrayAdapter<UsbSerialPort> mAdapter;

    private UsbManager mUsbManager;
    private UsbSerialPort mSerialPort;
    private ListView mListView;
    private TextView mProgressBarTitle;
    private ProgressBar mProgressBar;

    private static final int MESSAGE_REFRESH = 101;
    private static final long REFRESH_TIMEOUT_MILLIS = 5000;
    public static final String INTENT_ACTION_GRANT_USB = BuildConfig.APPLICATION_ID + ".GRANT_USB";

    private void refreshDeviceList() {
        showProgressBar();
        new AsyncTask<Void, Void, List<UsbSerialPort>>() {
            @Override
            protected List<UsbSerialPort> doInBackground(Void... params) {
                SystemClock.sleep(1000);
                final List<UsbSerialPort> result = new ArrayList<UsbSerialPort>();

                final List<UsbSerialDriver> drivers =
                        UsbSerialProber.getDefaultProber().findAllDrivers(mUsbManager);

                /*for (final UsbSerialDriver driver : drivers) {
                    final List<UsbSerialPort> ports = driver.getPorts();
                    Log.d(TAG, String.format("+ %s: %s port%s",
                            driver, Integer.valueOf(ports.size()), ports.size() == 1 ? "" : "s"));
                    result.addAll(ports);
                }*/
                return result;
            }

            @Override
            protected void onPostExecute(List<UsbSerialPort> result) {
                mEntries.clear();
                mEntries.addAll(result);
                //mAdapter.notifyDataSetChanged();
                //mProgressBarTitle.setText(String.format("%s device(s) found",Integer.valueOf(mEntries.size())));
                //hideProgressBar();
               // Log.d(TAG, "Done refreshing, " + mEntries.size() + " entries found.");
            }
        }.execute((Void) null);
    }

    private void showProgressBar() {
        mProgressBar.setVisibility(View.VISIBLE);
        mProgressBarTitle.setText(R.string.refreshing);
    }

    private void hideProgressBar() {
        mProgressBar.setVisibility(View.INVISIBLE);
    }

    private DeviceConfig() {
        mGson = new GsonBuilder().serializeNulls().create();

        Resources resources = App.getInstance().getResources();
        mBaudrates = resources.getIntArray(R.array.baudrates);
        mBaudrateStrs = new String[mBaudrates.length];
        for (int i = 0; i < mBaudrateStrs.length; i++) {
            mBaudrateStrs[i] = String.valueOf(mBaudrates[i]);
        }

        SerialPortFinder serialPortFinder = new SerialPortFinder();
        // Equipo
        String[] allDevicesPath = serialPortFinder.getAllDevicesPath();
        ArrayList<String> ttysXPahts = new ArrayList<>();

        if (allDevicesPath.length > 0) {
            String pattern = "^/dev/tty(S|(USB))\\d+$";
            for (String s : allDevicesPath) {
                if (Pattern.matches(pattern, s)) {
                    ttysXPahts.add(s);
                }
            }
        }

        if (ttysXPahts.size() > 0) {
            Collections.sort(ttysXPahts);
            mDevicePaths = ttysXPahts.toArray(new String[0]);
        } else {
            mDevicePaths = new String[] { "null" };
        }

        loadConfigFromFile();
        if (mConfig == null) {
            mConfig = new Config();
            mConfig.devicePath = mDevicePaths[0];
            mConfig.baudrate = 19200;
        }

        //refreshDeviceList();
    }

    public static DeviceConfig get() {
        return sInstance;
    }

    /**
     *   Guardar configuración en archivo, el valor predeterminado es asíncrono
     */
    private void saveConfigToFile() {
        saveConfigToFile(false);
    }

    /**
     * Guardar configuración en archivo
     *
     * @param sync Si guardar sincrónicamente
     */
    private void saveConfigToFile(boolean sync) {
        String json = mGson.toJson(mConfig);
        SharedPreferences.Editor editor = PrefUtil.getDefault().putString(CONFIG, json);
        if (sync) {
            editor.commit();
        } else {
            editor.apply();
        }
    }

    /**
     * Configuración de carga
     */
    private void loadConfigFromFile() {

        String json = PrefUtil.getDefault().getString(CONFIG, "");
        try {
            Config config = mGson.fromJson(json, Config.class);
            if (config != null) {
                mConfig = config;
            }
        } catch (JsonSyntaxException e) {
            e.printStackTrace();
        }
    }

    /**
     * Obtener lista de dispositivos
     *
     * @return
     */
    public String[] getDevicePaths() {
        return mDevicePaths;
    }

    /**
     * Obtener lista de velocidad de transmisión
     *
     * @return
     */
    public int[] getBaudrates() {
        return mBaudrates;
    }

    /**
     * Obtenga una lista de cadenas de velocidad de transmisión
     *
     * @return
     */
    public String[] getBaudrateStrs() {
        return mBaudrateStrs;
    }

    /**
     * Guardar el dispositivo
     *
     * @param devicePath
     * @param baudrate
     */
    public void updateSerialConfig(String devicePath, int baudrate) {
        mConfig.devicePath = devicePath;
        mConfig.baudrate = baudrate;
        saveConfigToFile();
    }

    /**
     * Guardar el dispositivo
     *
     * @param devicePath
     */
    public void updateSerialConfig(String devicePath) {
        mConfig.devicePath = devicePath;
        saveConfigToFile();
    }

    /**
     * Obtener ruta del dispositivo
     *
     * @return
     */
    public String getDevice() {
        return mConfig.devicePath;
    }

    /**
     * Obtenga la velocidad en baudios
     *
     * @return
     */
    public int getBaudrate() {
        return mConfig.baudrate;
    }

    /**
     * Encuentra el índice de ruta del dispositivo
     *
     * @param devicePath
     * @return
     */
    public int findDeviceIndex(String devicePath) {

        for (int i = 0; i < mDevicePaths.length; i++) {
            if (StringUtils.equals(mDevicePaths[i], devicePath)) {
                return i;
            }
        }

        return 0;
    }

    /**
     *
     Encuentra el índice de baudios
     *
     * @param baudrate
     * @return
     */
    public int findBaudrateIndex(int baudrate) {
        for (int i = 0; i < mBaudrates.length; i++) {
            if (mBaudrates[i] == baudrate) {
                return i;
            }
        }
        return 0;
    }

    /**
     *
     Si el dispositivo ha sido configurado
     *
     * @return
     */
    public boolean isDeviceConfiged() {
        return !TextUtils.isEmpty(mConfig.devicePath) && mConfig.baudrate != 0;
    }

    /**
     * Si habilitar la verificación CRC
     *
     * @return
     */
    public boolean isCrcEnable() {
        return mConfig.enableCrc;
    }

    /**
     * Configuración Habilitar / deshabilitar verificación CRC
     *
     * @param enable
     */
    public void setEnableCrc(boolean enable) {
        mConfig.enableCrc = enable;
        ModbusConfig.setEnableRtuCrc(enable);
        saveConfigToFile();
    }
}

