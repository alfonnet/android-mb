package com.licheedev.demo;

import android.content.Context;
import android.content.Intent;
import android.hardware.usb.UsbDevice;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import com.hoho.android.usbserial.driver.UsbSerialDriver;
import com.hoho.android.usbserial.driver.UsbSerialPort;
import com.licheedev.demo.base.BaseActivity;

public class ChooseModeActivity extends BaseActivity {

    private static UsbSerialPort sPort = null;
    @BindView(R.id.btn_serial_port)
    Button mBtnSerialPort;
    @BindView(R.id.btn_tcp)
    Button mBtnTcp;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_mode);
        ButterKnife.bind(this);
    }

    @OnClick({ R.id.btn_serial_port, R.id.btn_tcp })
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_serial_port:
                startActivity(MainActivity.newIntent(this, MainActivity.MODE_SERIAL, sPort));
                break;
            case R.id.btn_tcp:
                startActivity(MainActivity.newIntent(this, MainActivity.MODE_TCP, sPort));
                break;
        }
        finish();
    }

    /*public static Intent newIntent(Context context, @MainActivity.Mode int mode) {
        Intent intent = new Intent(context, MainActivity.class);
        intent.putExtra("mode", mode);
        return intent;
    }*/

    static void show(Context context, UsbSerialPort port) {
        sPort = port;

        final UsbSerialDriver driver = port.getDriver();
        final UsbDevice device = driver.getDevice();
        final String title = String.format("Vendor %4X Product %4X", device.getVendorId(), device.getProductId());
        final String subtitle = driver.getClass().getSimpleName();

        String portInfo =   "ENTRO AL SHOW MAIN: DATA : : ::  " + title;

        Toast.makeText(context, portInfo , Toast.LENGTH_SHORT).show();
        final Intent intent = new Intent(context, ChooseModeActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_NO_HISTORY);
        context.startActivity(intent);
    }
}
